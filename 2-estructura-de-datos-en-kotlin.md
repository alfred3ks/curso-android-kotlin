# Estructura de datos en Kotlin:

En Kotlin podemos manejar estos tipos de datos:

## Numéricos:

- Int: Representa un número entero de 32 bits.
- Long: Representa un número entero de 64 bits.
- Short: Representa un número entero de 16 bits.
- Byte: Representa un número entero de 8 bits.
- Float: Representa un número entero de 32 bits.
- Double: Representa un número de punto flotante de doble precisión de 64 bits

En Kotlin no existen los tipos primitivos como en Java.

```kotlin
fun main() {
    // Declaración de una variable Int:
    var edad: Int = 25
    println(edad)
}
```

Kotlin infiere el tipo de una variable, podemos obviar el tipo en la declaración de la variable. Lo recomendable es ponerlo.

## Carácter:

- Representa un caracter. En Kotlin los carcteres no son tratados como numeros como algunos otros lenguajes de programación.

## Booleano:

- Representa un valor verdadero o falso.

## Cadenas:

- Representa una cadena de caracteres.

## Arrays:

- Arrays<T>: Representan un arreglo de tipo T. Kotlin tambien proporsiona clases especualizadas para trabajar con arrays de tipo primitivos sin autoboxing para mayor eficiencia como IntArray, LogArray, etc.

## Colecciones:

- Kotlin cuenta con una rica biblioteca estandar que proporciona implementaciones de estructuras de datos comunes como List, Map, entre otras.

## Nullable Types (nulos):

- En Kotlin todos los tipos son no nulos por defecto. Sin embargo puedes declarar tipos anadiendo un ? al tipo. Por ejemplo: String? significa que puede contener una cadena o el valor null.

## Any:

- Es la superclase de todos los tipos no nulos en Kotlin. Equivalente al Object de Java.

## Unit:

- Representa el tipo retorno de una funcion que no devuelve nada util ( similar al void de Java).

## Nothing:

- Representa un valor que nunca regresa. Util para expresar operaciones que nunca completan.
