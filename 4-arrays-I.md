# Arrays I:

Tambien llamadas matrices, arreglos, es una estructura de datos que contiene una coleccion de valores del mismo tipo. En Kotlin no esta permitido los arrays de diferentes tipos.

Los usamos para guardar valores que normalmente tienen alguna relación entre si.

La sintaxis en Kotlin son las siguientes:

```kotlin
fun main() {
    // Primera forma de declarar un array en kotlinusando la funcion arrayOf():
    // Esta es la forma mas comun que podemos ver.
    // Podriamos obviar el tipo ya que Kotlin infiere el tipo.
    val mesesDelAño: Array<String> = arrayOf("Enero", "Febrero", "Marzo")
    println(mesesDelAño[0])
    println(mesesDelAño[1])
    println(mesesDelAño[2])

    // Con el metodos joinToString() podemos ver todo el contenido:
    println(mesesDelAño.joinToString())

    /*
    Segunda forma de declarar un array:
    Se declara un arreglo de 5 posiciones que tiene cinco ceros dentro.
    Estamos obligados en especificar el valor inicial.
    Mas adelante con las expresiones landan veremos que podemos especificar los valores
    exactos.
    */
    val miMatriz = Array(5){0} // Tenemos un array de cinco posiciones y ceros.

    // Con el metodos joinToString() podemos ver todo el contenido:
    println(miMatriz.joinToString())

    // Asi metemos valores al arreglo:
    miMatriz[0]=15
    miMatriz[1]=25
    miMatriz[2]=8
    miMatriz[3]=-7
    miMatriz[4]=92
    println(miMatriz[0])
    println(miMatriz[1])
    println(miMatriz[2])
    println(miMatriz[3])
    println(miMatriz[4])

    // Con el metodos joinToString() podemos ver todo el contenido:
    println(miMatriz.joinToString())

    // Mas adelante veremos bucles para recorrer el areglo.

    /*
    Aqui vemos otra forma de declarar arrays
    Valores enteros.
    Existe uno para cada tipo de valor: LongArraOf(), etc.
    */
    val miArr = intArrayOf(1,3,7,89,23)
    println(miArr.joinToString());

    // Asi declaramos un arreglo vacio: Debemos usar var en ves de val
    lateinit var myArr: Array<String>
}
```
