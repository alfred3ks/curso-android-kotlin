# Concatenación, variables y constantes en Kotlin:

Vamos aver como podemos hacer la concatenación de string y valores numericos, tambien veremos como se declaran las variables en Kotlin.

En Kotlin tenemos varias formas de hacer concatenación de string.

Las variables mutables en Kotlin de declaran usando la palabra reservada var.

Las variables inmutables en Kotlin se declaran usando la palabra reservada val.

En Kotlin lo que se conoce como constante a aquella que esta declarada a nivel de una clase. Eso lo veremos mas adelante cuando veamos las clases.

Para declararlas se haria asi:

const val nombrePersona = "Pepe"

A continuación vemos el ejercicio de string, variables mutables y inmutables:

```kotlin
fun main() {
  	// Concatenación de string:
    // Declaracion de una variable mutable:
    var ageJuan = 21
    // Usamos el operador suma para hacer concatenación:
    println("La edad de Juan es: " + ageJuan + " años.")
    println("Un año despues...")

    // Operador incremento:
    ageJuan++
    println("La edad de Juan es: " + ageJuan + " años.")

    // Operador decremento:
    ageJuan--
    // Concatenación de string. Los string templates. Esto es muy frecuente verlo:
    println("La edad de Juan es: $ageJuan años.")

    // Usamos la funcion .format: Interesante...
    println(String.format("La edad de Juan es: %d años", ageJuan))

    // Usando la funcion toString: Igual que la primera instrucción. Es lo mismo.
    println(String.format("La edad de Juan es: " + ageJuan.toString() + " años."))

    // Usamos la funcion: mmm Aqui si usamos las llaves {}
    println("La edad de Juan es: ${returnAge(ageJuan)} años.")

    // Una variable inmutable: Usamos la palabra reservada val:
    val PI: Double  = 3.1416
    println("El valor de PI es: " + PI);
}

// declaramos una función:
fun returnAge(age: Int):Int {
    return age;
}
```
